SELECT DISTINCT phone
FROM customers cust
JOIN orders ord ON ord.customerid = cust.customerid
JOIN orders_items ord_it ON ord_it.orderid = ord.orderid
JOIN products prod ON prod.sku = ord_it.sku
WHERE cust.citystatezip LIKE 'Queens Village%'
  AND prod.desc LIKE '%Senior Cat%';
