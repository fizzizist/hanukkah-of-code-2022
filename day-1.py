import pandas as pd
import numpy as np

NUM_PHONE_MAP = {2: {'a', 'b', 'c'},
                 3: {'d', 'e', 'f'},
                 4: {'g', 'h', 'i'},
                 5: {'j', 'k', 'l'},
                 6: {'m', 'n', 'o'},
                 7: {'p', 'q', 'r', 's'},
                 8: {'t', 'u', 'v'},
                 9: {'w', 'x', 'y', 'z'}}


def matching_phone_num(phone: str, name: str) -> bool:
    """Compares a phone number with a last name of the same length and return true if that name could be dialed on 
    a phone to match the given phone number.

    :param phone: The phone number containing dashes. E.G. '416-555-5555'
    :param name: a ten-letter last name.
    """
    phone = phone.replace('-', '')
    for i, num in enumerate(phone):
        if name[i].lower() not in NUM_PHONE_MAP.get(int(num), {}):
            return False
    return True


def main() -> None:
    # read in the CSV
    df = pd.read_csv('noahs-customers.csv')

    # parse the last names from the 'name' column
    df['last_name'] = np.vectorize(lambda name: name.split(' ')[1])(df['name'])

    # filter the df for only last names with exactly ten letters
    last_name_truth = np.vectorize(lambda name: len(name) == 10)(df['last_name'])
    df = df[last_name_truth]

    # filter the remaining data for names that match the phone number
    phone_match_truth = np.vectorize(matching_phone_num)(df['phone'], df['last_name'])
    df = df[phone_match_truth]

    print(df)


if __name__ == '__main__':
    main()
