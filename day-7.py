import pandas as pd
import numpy as np
import re

DAY_6_CUST = 8342


def get_matching_item(grp: pd.DataFrame) -> pd.DataFrame:
    day_6_items = grp[grp['customerid'] == DAY_6_CUST]['colorless']
    grp = grp.merge(day_6_items, on='colorless', how='inner')
    return grp


def main():
    order_df = pd.read_csv('noahs-orders.csv')
    order_items_df = pd.read_csv('noahs-orders_items.csv')
    customer_df = pd.read_csv('noahs-customers.csv')

    # get just those products that have a color
    product_df = pd.read_csv('noahs-products.csv')
    color_truth = np.vectorize(
        lambda sku, desc: sku[0:3] in {'COL', 'HOM'} and len(re.findall('\(.+\)', desc)) > 0
    )(product_df['sku'], product_df['desc'])
    product_df = product_df[color_truth]

    # remove the color from the product description
    product_df['colorless'] = np.vectorize(lambda desc: desc.split(' (')[0])(product_df['desc'])

    # filter orders on just the days that day-6 customer ordered these products on
    order_items_df = order_items_df.merge(product_df, on='sku', how='inner')[['orderid', 'colorless']]
    order_df = order_df.merge(order_items_df, on='orderid', how='inner')[['customerid', 'shipped', 'colorless']]
    order_df['shipped_date'] = np.vectorize(lambda shipped: shipped[:10])(order_df['shipped'])
    day_6_shipped = order_df[order_df['customerid'] == DAY_6_CUST]['shipped_date']
    order_df = order_df.merge(day_6_shipped, on='shipped_date', how='inner')

    # find orders that have matching items to day 6 customer for that day
    matching_df = order_df.groupby('shipped_date', group_keys=False).apply(get_matching_item).drop_duplicates()

    # merge customer data
    matching_df = matching_df.merge(
        customer_df, on='customerid', how='inner'
    )[['name', 'phone', 'shipped_date', 'colorless', 'phone']].sort_values(by=['shipped_date'])
    print(matching_df)


if __name__ == '__main__':
    main()
