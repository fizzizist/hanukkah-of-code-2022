SELECT phone
FROM customers
WHERE customerid in (
	SELECT customerid
	FROM (
		SELECT customerid, COUNT(*) ord_count
		FROM orders ord
		JOIN orders_items ord_it ON ord.orderid = ord_it.orderid
		JOIN products prod ON prod.sku = ord_it.sku
		WHERE ord_it.unit_price < prod.wholesale_cost
		GROUP BY customerid
		ORDER BY ord_count DESC
		LIMIT 1
	)
);
