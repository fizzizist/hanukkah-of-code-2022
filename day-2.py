# find customers with the initials 'JD' who ate coffee and bagels at noah's
import pandas as pd
import numpy as np


def get_initials(name: str) -> str:
    """Takes a name and returns just the initials in uppercase.

    :param name: A full name. E.G. Darris Johnson
    :return: Initals. E.G. DJ
    """
    name = name.split(' ')
    return ''.join([item[0].upper() for item in name])


def main():
    orders_items_df = pd.read_csv('noahs-orders_items.csv')
    orders_df = pd.read_csv('noahs-orders.csv')
    products_df = pd.read_csv('noahs-products.csv')
    cust_df = pd.read_csv('noahs-customers.csv')

    # get coffee and bagel products
    # upon manual inspection of the data, the filter terms were able to be refined here.
    coffee_bagel_truth = np.vectorize(
        lambda desc: 'Coffee, Drip' == desc or 'Bagel' in desc
    )(products_df['desc'])
    products_df = products_df[coffee_bagel_truth]

    # get just those order IDs with coffee and bagel products
    order_id_df = orders_items_df.merge(products_df, on='sku', how='inner')['orderid']

    # get just those customer IDs from the above order IDs
    cust_id_df = orders_df.merge(order_id_df, on='orderid', how='inner')['customerid']

    # merge those IDs with the customer table to filter it.
    cust_df = cust_df.merge(cust_id_df, on='customerid', how='inner')

    # filter cust DF for JD initials
    jd_truth = np.vectorize(lambda name: get_initials(name) == 'JD')(cust_df['name'])
    cust_df = cust_df[jd_truth].drop_duplicates()

    print(cust_df)


if __name__ == '__main__':
    main()
