SELECT phone
FROM customers
WHERE customerid in (
	SELECT customerid
	FROM (
		SELECT customerid, COUNT(*) cust_count
		FROM (
			SELECT customerid, MIN(ord.shipped), strftime ('%Y-%m-%d', ord.shipped) day
			FROM orders ord
			JOIN orders_items ord_it ON ord.orderid = ord_it.orderid
			JOIN products prod ON prod.sku = ord_it.sku
			WHERE prod.sku LIKE 'BKY%' AND ord.shipped < strftime('%Y-%m-%d 05:00:00', ord.shipped)
			GROUP BY day
		)
	GROUP BY customerid
	ORDER BY cust_count DESC
	LIMIT 1
	)
);
