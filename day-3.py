import pandas as pd
import numpy as np
from datetime import datetime

DATE_FMT = '%Y-%m-%d'
ARIES_RANGE = ('03-21', '04-19')
YEARS_OF_THE_DOG = ['1934', '1946', '1958', '1970', '1982', '1994']


def within_range(d: str) -> bool:
    d = datetime.strptime(d, DATE_FMT)
    for year in YEARS_OF_THE_DOG:
        lower = datetime.strptime(f'{year}-{ARIES_RANGE[0]}', DATE_FMT)
        upper = datetime.strptime(f'{year}-{ARIES_RANGE[1]}', DATE_FMT)
        if lower <= d <= upper:
            return True
    return False


def main():
    customer_df = pd.read_csv('noahs-customers.csv')
    aries_dog_truth = np.vectorize(within_range)(customer_df['birthdate'])
    aries_dog_df = customer_df[aries_dog_truth]
    nyc_truth = np.vectorize(
        lambda city_state: city_state.split(',')[0] == 'South Ozone Park'
    )(aries_dog_df['citystatezip'])
    print(aries_dog_df[nyc_truth][['citystatezip', 'phone']])
    print(len(aries_dog_df[nyc_truth]))


if __name__ == '__main__':
    main()
