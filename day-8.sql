SELECT phone, COUNT(ord.orderid) ord_count
FROM customers cust
JOIN orders ord ON ord.customerid = cust.customerid
JOIN orders_items ord_it ON ord_it.orderid = ord.orderid
WHERE ord_it.sku LIKE 'COL%' AND cust.citystatezip LIKE 'Manhattan%'
GROUP BY phone
ORDER BY ord_count DESC
LIMIT 1;
